package main

// TO BE USED ALONG WITH
// cangen vcan0 -g 200 -v

import (
	"encoding/binary"
	"log"
	"math"
	"time"

	"github.com/go-daq/canbus"
)

var send, err = canbus.New()

type CANPacket struct {
	SensorVal float64
	FrameID   uint32
}

var SensorBuf = make(chan CANPacket, 5)

func main() {
	startCAN()
}

func tempSensor() {
	for {
		SensorBuf <- CANPacket{
			math.Sin(float64(time.Now().UnixMilli())/1000)*100 + 100, //random sine generator
			2} //frame ID 2
		time.Sleep(time.Millisecond * 200)
	}
}
func pressureSensor() {
	for {
		SensorBuf <- CANPacket{
			math.Sin(float64(time.Now().UnixMilli())/1000+500)*100 + 100, //random sine generator @ 180* phase
			7} //frame ID 7
		time.Sleep(time.Millisecond * 200)
	}
}
func wheelRPMSensor() {
	for {
		SensorBuf <- CANPacket{
			math.Sin(float64(time.Now().UnixMilli())/1000+250)*50 + 50, //random sine generator @ 90* phase
			12} //frame ID 12
		time.Sleep(time.Millisecond * 200)
	}
}
func throttleSensor() {
	for {
		SensorBuf <- CANPacket{
			math.Sin(float64(time.Now().UnixMilli())/1000+750)*5 + 10, //random sine generator @ 270* phase
			10} //frame ID 10
		time.Sleep(time.Millisecond * 200)
	}
}

func startCAN() {
	go senderRoutine() // launch actual CAN communicator

	send, err = canbus.New()
	if err != nil {
		log.Fatal(err)
	}
	defer send.Close()

	err = send.Bind("vcan0")
	if err != nil {
		log.Fatalf("could not bind send socket: %+v", err)
	}

	// Call all sensing functions here which will comn through channels
	go tempSensor()     // start sending random values
	go pressureSensor() // start sending random values
	go wheelRPMSensor() // start sending random values
	go throttleSensor() // start sending random values

	for {
	}
}

func senderRoutine() {
	for {
		toSend := <-SensorBuf // wait for value from a sensor
		_, err := send.Send(canbus.Frame{
			ID:   toSend.FrameID,
			Data: []byte(Float64ToByte(float64(toSend.SensorVal))),
			Kind: canbus.SFF,
		})
		if err != nil {
			log.Fatalf("could not send frame %f: %+v", toSend.SensorVal, err)
		}
	}
}

func Float64ToByte(f float64) []byte {
	var buf [8]byte
	binary.BigEndian.PutUint64(buf[:], math.Float64bits(f))
	return buf[:]
}

func Float64FromBytes(bytes []byte) float64 {
	bits := binary.BigEndian.Uint64(bytes)
	float := math.Float64frombits(bits)
	return float
}
