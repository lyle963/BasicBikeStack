package main

import (
	"fmt"
	"strconv"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
)

func main() {

	BikeList := getBikes()

	a := app.New()
	w := a.NewWindow("Bike Admin")
	w.Resize(fyne.NewSize(600, 400))

	listView := widget.NewList(
		func() int {
			BikeList = getBikes()
			return len(BikeList)
		},
		func() fyne.CanvasObject {
			return widget.NewLabel("template")
		},
		func(id widget.ListItemID, object fyne.CanvasObject) {
			yyyy, mm, dd := BikeList[id].LastOnline.Date()
			textt := fmt.Sprintf("Bike %d ", BikeList[id].BikeNo) + fmt.Sprintf("(%d %0.3s %d)", dd, mm, yyyy)
			object.(*widget.Label).SetText(textt)
		})
	contenttext := widget.NewLabel("Select a Bike")

	listView.OnSelected = func(id widget.ListItemID) {
		s := fmt.Sprintf("Bike %v", BikeList[id].BikeNo)
		s += "\n\n"
		for _, v := range getParams(strconv.Itoa(int(BikeList[id].BikeNo))) {
			s += v.Field + " = " + fmt.Sprintf("%v", v.Value) + "\nAs of " + v.AsOf + "\n\n"
		}
		contenttext.SetText(s)
	}
	split := container.NewHSplit(
		listView,
		container.NewMax(contenttext),
	)
	split.Offset = 0.28
	w.SetContent(split)
	w.ShowAndRun()

}
