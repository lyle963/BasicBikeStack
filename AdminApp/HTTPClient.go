package main

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"time"
)

var IP string = "192.168.1.4"
var HTTPport string = "8080"

type response struct {
	BikeNo     uint32
	LastOnline time.Time
}

func getBikes() []response {
	var server_URL string = "http://" + IP + ":" + HTTPport
	resp, err := http.Get(server_URL + "/GetBikes")
	if err != nil {
		// handle error
		log.Println("s")
		return nil
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	res := []response{}
	json.Unmarshal([]byte(body), &res)
	// log.Printf("Got data from http getBikes: %v", res)
	return res
}

type Param struct {
	Field string
	Value float64
	AsOf  string
}

func getParams(BikeNo string) []Param {
	var server_URL string = "http://" + IP + ":" + HTTPport
	resp, err := http.Get(server_URL + "/GetParams?BikeNo=" + BikeNo)
	if err != nil {
		// handle error
		log.Println("s")
		return nil
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	res := []Param{}
	json.Unmarshal([]byte(body), &res)
	log.Printf("Got data from http getParams: %v", res)
	return res
}
