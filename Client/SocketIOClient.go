package main

import (
	"encoding/json"
	"fmt"
	"log"
	"strconv"

	socketio_client "github.com/zhouhui8915/go-socket.io-client"
)

func SocketConnect(SocketID string, BikeNo int) string {
	IsConnected := false

	opts := &socketio_client.Options{
		Transport: "websocket",
		Query:     make(map[string]string),
	}
	opts.Query["user"] = "userz"
	opts.Query["pwd"] = "passz"
	opts.Query["BikeNo"] = strconv.Itoa(BikeNo)
	opts.Query["socketID"] = SocketID
	uri := "http://" + IP + ":" + SocketIOport

	client, err := socketio_client.NewClient(uri, opts)
	if err != nil {
		log.Printf("NewClient error:%v\n", err.Error())
		log.Printf("NewClient err:%v\n", err)
		if err.Error() == "websocket: bad handshake" {
			IsConnected = false
			log.Printf("SAD")
			return "redohttp"
		}
		IsConnected = false
		return "error"
	}
	IsConnected = true

	client.On("error", func() {
		log.Printf("on error\n")
		IsConnected = false
	})
	client.On("connection", func() {
		log.Printf("on connect\n")
		IsConnected = true
	})
	client.On("message", func(msg string) {
		log.Printf("on message:%v\n", msg)
		IsConnected = true
	})
	client.On("disconnection", func() {
		log.Printf("on disconnect\n")
		IsConnected = false
	})
	// reader := bufio.NewReader(os.Stdin)
	for IsConnected {
		// data, _, _ := reader.ReadLine()
		// command := string(data)
		dataToSend := <-PostDataChannel
		json, err1 := json.Marshal(dataToSend)

		if err1 == nil {
			// fmt.Printf("val %s", string(json))
			client.Emit(SocketID, string(json))
		} else {
			fmt.Printf("err: %v\n", err.Error())
		}
	}
	return ""
}
