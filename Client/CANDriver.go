package main

import (
	"fmt"
	"log"

	"github.com/go-daq/canbus"
)

// pseudo enum for sensor types
const (
	temperature uint32 = 2
	pressure           = 7
	throttle           = 10
	wheelRPM           = 12
)

// create canbus obj in global memory
var send, _ = canbus.New()

func CANDriver() {
	recv, err := canbus.New()
	if err != nil {
		log.Fatal(err)
	}
	defer recv.Close()

	send, err = canbus.New()
	if err != nil {
		log.Fatal(err)
	}
	defer send.Close()

	err = recv.Bind("vcan0")
	if err != nil {
		log.Fatalf("could not bind recv socket: %+v", err)
	}

	err = send.Bind("vcan0")
	if err != nil {
		log.Fatalf("could not bind send socket: %+v", err)
	}

	// loop forever while we receive
	for {
		frame, err := recv.Recv()
		if err != nil {
			log.Fatalf("could not recv frame %d: %+v", 0, err)
		} else {
			// fmt.Printf("frame-: %q (id=0x%x)\n", frame.Data, frame.ID)
			go interpret(frame)
		}
	}
}

// Can call this function anytime to send data over CAN
func CANSend(val int64, ID int64) {
	_, err := send.Send(canbus.Frame{
		ID:   0x123,
		Data: []byte(fmt.Sprintf("data-%d", val)),
		Kind: canbus.SFF,
	})
	if err != nil {
		log.Fatalf("could not send frame %d: %+v", val, err)
	}
}

// take data and do things
func interpret(frame canbus.Frame) {
	switch frame.ID {
	case temperature:
		// fmt.Printf("Temperature = %f\n", Float64FromBytes(frame.Data))
		SensorAnalytics(frame.ID, Float64FromBytes(frame.Data))
	case pressure:
		// fmt.Printf("Pressure = %f\n", Float64FromBytes(frame.Data))
		SensorAnalytics(frame.ID, Float64FromBytes(frame.Data))
	case throttle:
		// fmt.Printf("Throttle = %f\n", Float64FromBytes(frame.Data))
		SensorAnalytics(frame.ID, Float64FromBytes(frame.Data))
	case wheelRPM:
		// fmt.Printf("RPM = %f\n", Float64FromBytes(frame.Data))
		SensorAnalytics(frame.ID, Float64FromBytes(frame.Data))
	default:
		// fmt.Printf("Frame ID [%v]= %f\n", frame.ID, Float64FromBytes(frame.Data))
		SensorAnalytics(frame.ID, Float64FromBytes(frame.Data))
	}
}
