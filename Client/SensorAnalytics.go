package main

import (
	"encoding/binary"
	"math"
	"time"
)

type Data struct {
	Key   string
	Value float64
}

var PostDataChannel = make(chan Data, 10)

func SensorAnalytics(sensorType uint32, sensorValue float64) {
	switch sensorType {
	case temperature:
		PostDataChannel <- Data{"temp", compute(sensorValue)}
	case pressure:
		PostDataChannel <- Data{"press", compute(sensorValue)}
	case wheelRPM:
		PostDataChannel <- Data{"wRPM", compute(sensorValue)}
	case throttle:
		PostDataChannel <- Data{"thr", compute(sensorValue)}
	}

}

func compute(val float64) float64 {
	if val < 3 {
		val = 3 //random logic to not allow below 3
	}
	time.Sleep(time.Millisecond * 500) // assume it takes 500s to process the data
	return val
}

func Float64ToByte(f float64) []byte {
	var buf [8]byte
	binary.BigEndian.PutUint64(buf[:], math.Float64bits(f))
	return buf[:]
}

func Float64FromBytes(bytes []byte) float64 {
	bits := binary.BigEndian.Uint64(bytes)
	float := math.Float64frombits(bits)
	return float
}
