package main

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"strconv"
)

type response struct {
	SocketID string
}

func GetSocketID(BikeNo int, BikeKey string) string {
	var server_URL string = "http://" + IP + ":" + HTTPport

	resp, err := http.Get(server_URL + "/GetSocket?BikeNo=" + strconv.Itoa(BikeNo) + "&BikeKey=" + BikeKey)
	if err != nil {
		// handle error
		log.Println("s")
		return ""
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	res := response{}
	json.Unmarshal([]byte(body), &res)
	log.Printf("Got socketID from http: %v", res.SocketID)
	return res.SocketID
}
