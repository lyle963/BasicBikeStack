module Cloud

go 1.19

require (
	github.com/go-daq/canbus v0.2.0
	github.com/zhouhui8915/go-socket.io-client v0.0.0-20200925034401-83ee73793ba4
)

require (
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/smartystreets/goconvey v1.7.2 // indirect
	github.com/zhouhui8915/engine.io-go v0.0.0-20150910083302-02ea08f0971f // indirect
	golang.org/x/sys v0.0.0-20220818161305-2296e01440c6 // indirect
)
