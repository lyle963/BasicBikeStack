package main

import (
	"log"
	"time"
)

var IP string = "192.168.1.4"
var HTTPport string = "8080"
var SocketIOport string = "8000"

func main() {
	BikeNo := 1
	BikeKey := "KEY1"

	go CANDriver()

	socketID := GetSocketID(BikeNo, BikeKey)
	log.Printf("socketID=%v", socketID)
	for { // incase server dies, loop
		if SocketConnect(socketID, BikeNo) != "" {
			socketID := GetSocketID(BikeNo, BikeKey)
			log.Printf("Did http, socketID=%v", socketID)
		}
		time.Sleep(time.Second * 3)
	}
}
