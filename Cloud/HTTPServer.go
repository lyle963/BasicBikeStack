package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"sync"
	"time"
)

func StartHttp() {
	fmt.Println("Starting HTTP Server")
	http.Handle("/GetSocket", new(GetSocketHandler)) // todo merge all into 1 and modularise
	http.Handle("/GetBikes", new(GetBikesHandler))
	http.Handle("/GetParams", new(GetParamsHandler))
	log.Fatal(http.ListenAndServe(":8080", nil))
}

type GetSocketHandler struct {
	mu sync.Mutex // guards Bikes TODO check on this. Can I use mutex in bike struct
}

type GetParamsHandler struct {
	// mu sync.Mutex // guards Bikes TODO check on this. Can I use mutex in bike struct
}

type GetBikesHandler struct {
	// mu sync.Mutex // guards Bikes TODO check on this. Can I use mutex in bike struct
}

// GetSocket API
// Takes BikeNo and BikeKey and responds with SocketID
func (h *GetSocketHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	Query := r.URL.Query()
	NewSocketID := ""
	for i, b := range Bikes {
		if fmt.Sprint(b.BikeNo) == Query.Get("BikeNo") {
			if b.BikeKey == Query.Get("BikeKey") {
				if b.SocketID == "" {
					for {
						IsAlreadyTaken := false //assume not a duplicate
						NewSocketID = fmt.Sprint(rand.Intn(80000)) + "SOCK" + fmt.Sprint(rand.Intn(99999))
						for _, b2 := range Bikes {
							if NewSocketID == b2.SocketID {
								IsAlreadyTaken = true //duplicate found
								break
							}
						}
						if IsAlreadyTaken == false {
							break
						}
					}
					h.mu.Lock()
					defer h.mu.Unlock()
					Bikes[i].SocketID = NewSocketID
					server.OnEvent("/", NewSocketID, MSGOnSocketID)
				} else {
					NewSocketID = b.SocketID //New is same as old
					h.mu.Lock()
					defer h.mu.Unlock()
					CloseSocket(Bikes[i].socketConn) //disconnect old sockets from same bike
					break
				}
				break
			}
		}
	}
	type SocketIDJSON struct {
		SocketID string
	}
	socketIDOBJ := SocketIDJSON{NewSocketID}
	response, _ := json.Marshal(socketIDOBJ)
	responseString := string(response)
	fmt.Fprintf(w, responseString)
}

// GetBikes  API
func (h *GetBikesHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	type allbikeslistitem struct {
		BikeNo     uint32
		LastOnline time.Time
	}
	AllBikesObj := []allbikeslistitem{}

	for _, b := range Bikes {
		AllBikesObj = append(AllBikesObj, allbikeslistitem{
			b.BikeNo,
			b.LastOnline,
		})
	}

	response, _ := json.Marshal(AllBikesObj)
	responseString := string(response)
	fmt.Fprintf(w, responseString)
}

// GetParams  API
func (h *GetParamsHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	Query := r.URL.Query()
	BikeNo := Query.Get("BikeNo")
	params := DBReadParams(BikeNo)
	response, _ := json.Marshal(params)
	responseString := string(response)
	fmt.Fprintf(w, responseString)
}
