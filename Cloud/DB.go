package main

import (
	"context"
	"log"
	"os"
	"strconv"
	"time"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
	"github.com/influxdata/influxdb-client-go/v2/api"
	"github.com/influxdata/influxdb-client-go/v2/api/write"
)

var writeAPI api.WriteAPIBlocking
var client influxdb2.Client
var org string

func DBsetup() {
	token := os.Getenv("INFLUXDB_TOKEN")
	token = "EJKj2mIPZ7bRp1s8NZ38fnUs7xE7go2yhtcF9dmeoTOTu8xUJ7aB6jqA85ZPmngCjdCEejxSfA8AT5vzS7aRyw=="
	url := "http://localhost:8086"
	client = influxdb2.NewClient(url, token)

	org = "LyleProjects"
	bucket := "BikeBucket"
	writeAPI = client.WriteAPIBlocking(org, bucket)
}

func DBwrite(BikeNo uint32, fields map[string]interface{}) {
	tags := map[string]string{
		"BikeNo": strconv.Itoa(int(BikeNo)),
	}
	point := write.NewPoint("Telemetry", tags, fields, time.Now())
	if err := writeAPI.WritePoint(context.Background(), point); err != nil {
		log.Fatal(err)
	}
}

type Param struct {
	Field string
	Value float64
	AsOf  string
}

func DBReadParams(BikeNo string) []Param {
	queryAPI := client.QueryAPI(org)
	query := `from(bucket: "BikeBucket")
			|> range(start: -100d)
            |> filter(fn: (r) => r._measurement == "Telemetry")
			|> filter(fn: (r) => r.BikeNo == "` + BikeNo + `")
			|> last()`

	results, err := queryAPI.Query(context.Background(), query)
	if err != nil {
		log.Fatal(err)
	}

	LatestData := []Param{}
	for results.Next() {
		res := results.Record()
		LatestData = append(LatestData, Param{
			res.Field(),
			res.Value().(float64),
			res.Time().Local().String(),
		})
	}
	if err := results.Err(); err != nil {
		log.Fatal(err)
		return nil
	}
	return LatestData
}
