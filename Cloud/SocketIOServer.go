package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/url"
	"strconv"
	"time"

	socketio "github.com/googollee/go-socket.io"
	"github.com/labstack/echo"
)

var server *socketio.Server

func SetupSocketServer() {

	server = socketio.NewServer(nil)
	server.OnConnect("/", func(s socketio.Conn) error {
		s.SetContext("")
		log.Println("connected:", s.ID())
		qwery, _ := url.ParseQuery(s.URL().RawQuery)
		BikeNo := qwery.Get("BikeNo")
		socketID := qwery.Get("socketID")
		isLinked := false
		for i, b := range Bikes {
			//disconnect immediately incase BikeNo/socketID are not matching DB
			// fmt.Println(b)
			if strconv.Itoa(int(b.BikeNo)) == BikeNo && b.SocketID == socketID && b.SocketID != "" {
				Bikes[i].mu.Lock()
				CloseSocket(Bikes[i].socketConn)
				Bikes[i].socketConn = &s
				Bikes[i].LastOnline = time.Now()
				isLinked = true
				Bikes[i].mu.Unlock()
				break
			}
		}
		if isLinked == false {
			s.Close()
		}
		return nil
	})

	server.OnEvent("/", "message", func(s socketio.Conn, msg string) {
		// log.Println("notice:", msg)
		s.Emit("message", "have "+msg)
	})

	server.OnEvent("/", "bye", func(s socketio.Conn) string {
		last := s.Context().(string)
		s.Emit("bye", last)
		s.Close()
		return last
	})

	server.OnError("/", func(s socketio.Conn, e error) {
		log.Println("meet error:", e, s.ID())
		fmt.Printf("Active sox=%v\n", server.Count())
	})

	server.OnDisconnect("/", func(s socketio.Conn, reason string) {
		log.Println("closed", reason, s.ID())
		fmt.Printf("Active sox=%v\n", server.Count()-1)
	})
}

func StartSocketServer() {

	go server.Serve()
	defer server.Close()

	e := echo.New()
	e.HideBanner = true

	e.Static("/", "../asset")
	e.Any("/socket.io/", func(context echo.Context) error {
		server.ServeHTTP(context.Response(), context.Request())
		return nil
	})
	e.Logger.Fatal(e.Start(":8000"))
}

// On each SocketID, handler will be created
func MSGOnSocketID(s socketio.Conn, msg string) {
	qwery, _ := url.ParseQuery(s.URL().RawQuery)
	BikeNo := qwery.Get("BikeNo")
	socketID := qwery.Get("socketID")
	s.Emit("message", "ACK")
	// log.Println(msg)
	for i, b := range Bikes {
		//disconnect immediately incase bikeno/socket are not matching DB
		if strconv.Itoa(int(b.BikeNo)) == BikeNo && b.SocketID == socketID && b.SocketID != "" {
			Bikes[i].mu.Lock()
			Bikes[i].LastOnline = time.Now()
			Bikes[i].mu.Unlock()

			var dat map[string]interface{} // todo learn this

			if err := json.Unmarshal([]byte(msg), &dat); err != nil {
				panic(err)
			}

			field := dat["Key"].(string)
			value := dat["Value"]
			fields := map[string]interface{}{
				field: value,
			}
			// fmt.Printf("BikeNo %v sent %s over %s on ch %v -[STORE]\n", BikeNo, msg, socketID, s.ID())
			DBwrite(1, fields)

			break
		}
	}
}

func CloseSocket(skptr *socketio.Conn) {
	last := ""
	if skptr != nil {
		s := *skptr
		if s != nil {
			last = s.Context().(string)
			s.Emit("message", "bye")
			s.Close()
		}
	}
	fmt.Printf("Active sox=%v -> %v\n", server.Count(), last)
}
