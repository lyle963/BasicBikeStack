package main

import (
	"fmt"
	"sync"
	"time"

	socketio "github.com/googollee/go-socket.io"
)

type bike struct {
	BikeNo     uint32
	BikeKey    string
	LastOnline time.Time
	SocketID   string
	socketConn *socketio.Conn
	mu         sync.Mutex
}

var Bikes []bike

func addbikes() { // Add fake bikes to RAM database
	fmt.Println("Adding fake bikes as if from database")
	Bikes = append(Bikes, bike{1, "KEY1", time.Now(), "", nil, sync.Mutex{}})
	Bikes = append(Bikes, bike{2, "KEY2", time.Now(), "", nil, sync.Mutex{}})
}

func main() {
	DBsetup()
	addbikes()
	SetupSocketServer()
	go StartHttp()      // Listen on HTTP Server
	StartSocketServer() // Start Socket Server
}
